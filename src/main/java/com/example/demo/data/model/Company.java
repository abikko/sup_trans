package com.example.demo.data.model;

public class Company {
    private String companyName;
    private String bin;
    private String country;
    private String city;
    private String address;
    private String fullName;
    private String username;
    private String email;
    private String password;

    public Company(String companyName, String bin, String country, String city, String address, String fullName, String username, String email, String password) {
        this.companyName = companyName;
        this.bin = bin;
        this.country = country;
        this.city = city;
        this.address = address;
        this.fullName = fullName;
        this.username = username;
        this.email = email;
        this.password = password;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getBin() {
        return bin;
    }

    public void setBin(String bin) {
        this.bin = bin;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
