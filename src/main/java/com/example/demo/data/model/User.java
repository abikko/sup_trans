package com.example.demo.data.model;

public class User {
    private Company company;
    private UnregisteredUser unregisteredUser;
    private RegisteredUser registeredUser;

    public User(Company company) {
        this.company = company;
    }

    public User(UnregisteredUser unregisteredUser) {
        this.unregisteredUser = unregisteredUser;
    }

    public User(RegisteredUser registeredUser) {
        this.registeredUser = registeredUser;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public UnregisteredUser getUnregisteredUser() {
        return unregisteredUser;
    }

    public void setUnregisteredUser(UnregisteredUser unregisteredUser) {
        this.unregisteredUser = unregisteredUser;
    }

    public RegisteredUser getRegisteredUser() {
        return registeredUser;
    }

    public void setRegisteredUser(RegisteredUser registeredUser) {
        this.registeredUser = registeredUser;
    }
}
