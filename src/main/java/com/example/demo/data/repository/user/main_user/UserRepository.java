package com.example.demo.data.repository.user.main_user;

import com.example.demo.data.model.User;

import java.util.ArrayList;

public interface UserRepository {
    ArrayList<User> getUsers();
}
