package com.example.demo.data.repository.user.main_user;

import com.example.demo.data.model.RegisteredUser;
import com.example.demo.data.model.User;

import java.util.ArrayList;

public class UserRepositoryImpl implements UserRepository {
    @Override
    public ArrayList<User> getUsers() {
        ArrayList<User> users = new ArrayList<>();
        users.add(new User(new RegisteredUser("zhetesoft",
                "12313", "asd",
                "astana", "abyl",
                "abylaikhan", "zhetesoft",
                "abikzh@gmail.com", "asdasd123")));
        users.add(new User(new RegisteredUser("12321",
                "123321333332313", "asdad",
                "astanaaaa", "abyaaal",
                "abylaaaaikhan", "zhetesoaaaft",
                "abikzh@gmailaaa.com", "asdasd1aaa23")));
        users.add(new User(new RegisteredUser("",
                "123aaaa13", "aaaasd",
                "astanaaaa", "aaaabyl",
                "abylaaaaikhan", "aaazhetesoft",
                "abikzh@gaaaamail.com", "aaaasdasd123")));
        return users;
    }
}
