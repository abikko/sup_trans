package com.example.demo.ui.user.welcome_page;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

//our controller
@WebServlet(name = "welcomePageUser", value = "/welcomePageUser")
public class WelcomePageUser extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String clickedButtonName = req.getParameter("button");
        System.out.println(clickedButtonName);
        String nextUrl = "";
        switch (clickedButtonName){
            case "registration":
                nextUrl = "/demo_war_exploded/userRegistrationPage";
                break;
            case "login":
                nextUrl = "/demo_war_exploded/userLoginPage";
                break;
        }
        resp.sendRedirect(nextUrl);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.getRequestDispatcher("/welcomePage/index.jsp").forward(request, response);
    }
}